set foreign_key_checks=0;

--
-- Database
--
-- CREATE DATABASE Atelier_1KAFEY;
-- USE Atelier_1KAFEY;

--
-- Structure de la table Type
--

CREATE TABLE `Type` (
  `id_typ` int(11) AUTO_INCREMENT PRIMARY KEY,
  `nom_typ` varchar(128) NOT NULL
)ENGINE=INNODB;

--
-- Contenu de la table Type
--

INSERT INTO `Type` (`id_typ`, `nom_typ`) VALUES
('', 'CD'),
('', 'DVD'),
('', 'Livre');

-- --------------------------------------------------------

--
-- Structure de la table `Genre`
--

CREATE TABLE `Genre` (
  `id_gen` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `nom_gen` varchar(128) NOT NULL
) ENGINE=INNODB;

--
-- Contenu de la table `Genre`
--

INSERT INTO `Genre` (`id_gen`, `nom_gen`) VALUES
('', 'action'),
('', 'aventure'),
('', 'classique'),
('', 'policier'),
('', 'romance');


-- --------------------------------------------------------

--
-- Structure de la table `item`
--

CREATE TABLE `Item` (
  `id_ite` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `titre_ite` varchar(128) NOT NULL,
  `desc_ite` varchar(128) NOT NULL,
  `id_gen` varchar(128) NOT NULL,
  `id_typ` varchar(128) NOT NULL,
  FOREIGN KEY (id_gen) REFERENCES Genre(id_gen),
  FOREIGN KEY (id_typ) REFERENCES Type(id_typ)
)ENGINE=INNODB;

--
-- Contenu de la table `item`
--

INSERT INTO `item` (`id_ite`, `titre_ite`, `desc_ite`, `id_gen`, `id_typ`) VALUES
('', 'Titre1', 'Desc1', '1', '1'),
('', 'Titre2', 'Desc2', '2', '2'),
('', 'Titre3', 'Desc3', '3', '2'),
('', 'Titre4', 'Desc4', '4', '3'),


-- --------------------------------------------------------

--
-- Structure de la table Etat
--

CREATE TABLE `Etat` (
  `id_eta` int(11) AUTO_INCREMENT PRIMARY KEY,
  `nom_eta` varchar(128) NOT NULL
)ENGINE=INNODB;

--
-- Contenu de la table Etat
--

INSERT INTO `Etat` (`id_eta`, `nom_eta`) VALUES
('', 'Administrateur'),
('', 'Adherant');


-- --------------------------------------------------------

--
-- Structure de la table `Membre`
--

CREATE TABLE `Membre` (
  `id_mem` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `nom_mem` varchar(128) NOT NULL,
  `pnom_memn` varchar(128) NOT NULL,
  `mdp_mem` varchar(128) NOT NULL,
  `mail_mem` varchar(128) NOT NULL,
  `id_eta` int(1) NOT NULL,
  `adr_mem` varchar(128) NOT NULL,
  `cp_mem` int(5) NOT NULL,
  `ville_mem` varchar(128) NOT NULL,
  `numTel_mem` int(10) NOT NULL,
  FOREIGN KEY (id_eta) REFERENCES Etat(id_eta)
)ENGINE=INNODB;

--
-- Contenu de la table `Membre`
--

-- '5a105e8b9d40e1329780d62ea2265d8a' -> "test1"
-- 'ad0234829205b9033196ba818f7a872b' -> "test2"
-- '8ad8757baa8564dc136c1e07507f4a98' -> "test3"
-- '86985e105f79b95d6bc918fb45ec7727' -> "test4"

INSERT INTO `Membre` (`id_mem`, `nom_mem`, `prenom_ven`, `mdp_mem`, `mail_mem`, `id_eta`, `adr_mem`, `cp_mem`, `ville_mem`, `numTel_mem`) VALUES
('', 'Nom1', 'Prenom1', '5a105e8b9d40e1329780d62ea2265d8a', 'mail1@example.fr', '2', '1 Rue des example', '01234', 'Nancy', '0123456789'),
('', 'Nom2', 'Prenom2', 'ad0234829205b9033196ba818f7a872b', 'mail2@example.fr', '1', '2 Rue des example', '12345', 'Paris', '1234567890'),
('', 'Nom3', 'Prenom3', '8ad8757baa8564dc136c1e07507f4a98', 'mail3@example.fr', '1', '3 Rue des example', '23456', 'Bordeaux', '2345678901'),
('', 'Nom4', 'Prenom4', '86985e105f79b95d6bc918fb45ec7727', 'mail4@example.fr', '2', '4 Rue des example', '34567', 'Lille', '3456789012');

-- --------------------------------------------------------

--
-- Structure de la table `Location`
--

CREATE TABLE `Location` (
  `id_loc` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_mem` int(11) NOT NULL,
  `id_ite` int(11) NOT NULL,
  `dateDeb_loc` DATE NOT NULL,
  `dateFin_loc` DATE NOT NULL,
  FOREIGN KEY (id_mem) REFERENCES Membre(id_mem),
  FOREIGN KEY (id_ite) REFERENCES Item(id_ite)
)ENGINE=INNODB;

--
-- Contenu de la table `Location`
--

INSERT INTO `Location` (`id_loc`, `id_mem`, `id_ite`, `dateDeb_loc`, `dateFin_loc`) VALUES
('', '1', '1', '2015-11-01', '2015-11-02'),
('', '2', '2', '2015-11-01', '2015-11-03'),
('', '3', '3', '2015-11-01', '2015-11-04');

-- --------------------------------------------------------

--
-- Structure de la table `Reservation`
--

CREATE TABLE `Reservation` (
  `id_res` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_mem` int(11) NOT NULL,
  `id_ite` int(11) NOT NULL,
  `dateDeb_res` DATE NOT NULL,
  `dateFin_res` DATE NOT NULL,
  FOREIGN KEY (id_mem) REFERENCES Membre(id_mem),
  FOREIGN KEY (id_ite) REFERENCES Item(id_ite)
)ENGINE=INNODB;

--
-- Contenu de la table `Reservation`
--

INSERT INTO `Location` (`id_res`, `id_mem`, `id_ite`, `dateDeb_res`, `dateFin_res`) VALUES
('', '3', '1', '2015-11-04', '2015-11-05'),
('', '1', '2', '2015-11-04', '2015-11-06'),
('', '2', '3', '2015-11-04', '2015-11-07');
