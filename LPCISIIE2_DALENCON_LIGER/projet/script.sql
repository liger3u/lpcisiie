set foreign_key_checks=0;

--
-- Structure de la table Ville
--

CREATE TABLE `Ville` (
  `id_vil` int(11) AUTO_INCREMENT PRIMARY KEY,
  `nom_vil` varchar(80) NOT NULL
)ENGINE=INNODB;

--
-- Contenu de la table Ville
--

INSERT INTO `Ville` (`id_vil`, `nom_vil`) VALUES
('', 'Nancy'),
('', 'Lyon'),
('', 'Rennes'),
('', 'Bordeaux'),
('', 'Paris');

-- --------------------------------------------------------

--
-- Structure de la table `Quartier`
--

CREATE TABLE `Quartier` (
  `id_qua` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `nom_qua` varchar(128) NOT NULL,
  `id_vil` int(11) NOT NULL,
  FOREIGN KEY (id_vil) REFERENCES ville(id_vil)
) ENGINE=INNODB;

--
-- Contenu de la table `Quartier`
--

INSERT INTO `Quartier` (`id_qua`, `nom_qua`, `id_vil`) VALUES
('', 'gare', '1'),
('', 'leopold', '1'),
('', 'Bellecour', '2'),
('', 'Croix-rousse', '2'),
('', 'Ferrer', '3'),
('', 'Centre commercial', '3'),
('', 'Caudéran', '4'),
('', 'Bastide', '4'),
('', '16eme', '5'),
('', '18eme', '5');

-- --------------------------------------------------------

--
-- Structure de la table `Type_Bien`
--

CREATE TABLE `Type_Bien` (
  `id_typ` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `nom_typ` varchar(128) NOT NULL
)ENGINE=INNODB;

--
-- Contenu de la table `Type_Bien`
--

INSERT INTO `Type_Bien` (`id_typ`, `nom_typ`) VALUES
('', 'Maison'),
('', 'Appartement'),
('', 'Garage'),
('', 'Terrain');

-- --------------------------------------------------------

--
-- Structure de la table `Vendeur`
--

CREATE TABLE `Vendeur` (
  `id_ven` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `numTel_ven` int(10) NOT NULL,
  `adresse_ven` varchar(128) NOT NULL,
  `mail_ven` varchar(128) NOT NULL,
  `nom_ven` varchar(128) NOT NULL,
  `prenom_ven` varchar(128) NOT NULL,
  `type_ven` varchar(128) NOT NULL,
  `pass_ven` varchar(128) NOT NULL
)ENGINE=INNODB;

--
-- Contenu de la table `Vendeur`
--

-- '5a105e8b9d40e1329780d62ea2265d8a' -> "test1"
-- 'ad0234829205b9033196ba818f7a872b' -> "test2"
-- '8ad8757baa8564dc136c1e07507f4a98' -> "test3"
-- '86985e105f79b95d6bc918fb45ec7727' -> "test4"

INSERT INTO `Vendeur` (`id_ven`, `numTel_ven`, `adresse_ven`, `mail_ven`, `nom_ven`, `prenom_ven`, `type_ven`, `pass_ven`) VALUES
('', '8123456789', '1 rue de la gare 54000 Nancy', 'zinedine.zidane@gmail.fr', 'ZIDANE', 'Zinedine', 'particulier', '5a105e8b9d40e1329780d62ea2265d8a'),
('', '9876543275', '1 bld Bellecour 69000 Lyon', 'fabien.barthez@gmail.fr', 'BARTHEZ', 'Fabien', 'agence', 'ad0234829205b9033196ba818f7a872b'),
('', '9685743021', '1 rue du Ferrer 35000 Rennes', 'emmanuel.petit@gmail.fr', 'PETIT', 'Emmanuel', 'particulier', '8ad8757baa8564dc136c1e07507f4a98'),
('', '5425845684', '1 rue du Bastide 33000 Bordeaux', 'david.trezeguet@gmail.fr', 'TREZEGUET', 'David', 'particulier', '86985e105f79b95d6bc918fb45ec7727');

-- --------------------------------------------------------

--
-- Structure de la table `Annonce`
--

CREATE TABLE `Annonce` (
  `id_ann` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `titre_ann` varchar(128) NOT NULL,
  `desc_ann` varchar(128) NOT NULL,
  `image_ann` varchar(128) NOT NULL,
  `id_qua` int(11) NOT NULL,
  `superficie_ann` int(11) NOT NULL,
  `prix_ann` int(11) NOT NULL,
  `id_ven` int(11) NOT NULL,
  `id_typ` int(11) NOT NULL,
  `nbpiece_ann` int(2) NOT NULL,
  `date_ann` DATE NOT NULL,
  `loc_ann` varchar(128) NOT NULL,
  FOREIGN KEY (id_qua) REFERENCES quartier(id_qua),
  FOREIGN KEY (id_ven) REFERENCES vendeur(id_ven),
  FOREIGN KEY (id_typ) REFERENCES type_bien(id_typ)
)ENGINE=INNODB;

--
-- Contenu de la table `Annonce`
--

INSERT INTO `Annonce` (`id_ann`, `titre_ann`, `desc_ann`, `image_ann`, `id_qua`, `superficie_ann`, `prix_ann`, `id_ven`, `id_typ`, `nbpiece_ann`, `date_ann`, `loc_ann`) VALUES
('', 'Vend maison rustique', 'belle maison', 'img/maison.png', '1', '400', '1234', '1', '1', '5', '2015-10-18', 'Vente'),
('', 'Loue Appartement 2 pièces', 'bel immeuble', 'img/appartement.png', '3', '40', '450', '2', '2', '2', '2015-10-20', 'Location'),
('', 'Loue garage privé et fermé', 'beau garage', 'img/garage.png', '5', '10', '100', '3', '3', '1', '2015-10-19', 'Location'),
('', 'Vend terrain constructible non innondable', 'beau terrain', 'img/terrain.png', '7', '700', '4000', '4', '4', '0', '2015-10-21', 'Vente');
