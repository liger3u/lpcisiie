<?php

use app\model\Annonce as Annonce;
use app\model\Ville as Ville;
use app\model\Quartier as Quartier;
use app\model\Type_Bien as TypeBien;
use app\model\Vendeur as Vendeur;
use Illuminate\Database\Capsule\Manager as DB;


/* ACCUEIL */
$app->get('/', function () use ($app) { $app->render('accueil.twig'); });


/* DEPOSER ANNONCE */
$app->get('/deposer_une_annonce', function () use ($app) { $app->render('vendeur.twig'); });

$app->post('/etape_1', function () use ($app)
  {
  $req = $app->request();
  $val = str_replace('"', "", json_encode($req->post('create')));
  $vendeur = Vendeur::all();

  if($val == "oui") $app->render('dua_create_ven.twig',  array('ville' => Ville::orderBy('nom_vil')->get(), 'typeBien' => TypeBien::orderBy('nom_typ')->get()));
  else $app->render('dua_ven.twig', array('ville' => Ville::orderBy('nom_vil')->get(), 'vendeur' => $vendeur, 'typeBien' => TypeBien::orderBy('nom_typ')->get()));

  });

$app->post('/ajout_annonce_vendeur', function () use ($app)
  {
  $req = $app->request();

  $vide = FALSE; // boolean TRUE si une valeur est vide ou null.
  /* données annonce */
  if(empty($tit_a = str_replace('"', "", json_encode($req->post('titre')))) || $tit_a == "") $vide = TRUE;
  if(empty($des_a = str_replace('"', "", json_encode($req->post('desc')))) || $des_a == "") $vide = TRUE;
  if(empty($qua_a = str_replace('"', "", json_encode($req->post('quartier')))) || $qua_a == "") $vide = TRUE;
  if(empty($vil_a = str_replace('"', "", json_encode($req->post('ville')))) || $vil_a == "") $vide = TRUE;
  if(empty($sup_a = str_replace('"', "", json_encode($req->post('superficie')))) || $sup_a == "") $vide = TRUE;
  if(empty($nbp_a = str_replace('"', "", json_encode($req->post('nbpiece')))) || $nbp_a == "") $vide = TRUE;
  if(empty($pri_a = str_replace('"', "", json_encode($req->post('prix')))) || $pri_a == "") $vide = TRUE;
  if(empty($typ_a = str_replace('"', "", json_encode($req->post('type')))) || $typ_a == "") $vide = TRUE;
  if(empty($loc_a = str_replace('"', "", json_encode($req->post('loc')))) || $loc_a == "") $vide = TRUE;

  /* données vendeur */
  if(empty($nom_v = str_replace('"', "", json_encode($req->post('nom')))) || $nom_v == "") $vide = TRUE;
  if(empty($pnom_v = str_replace('"', "", json_encode($req->post('pnom')))) || $pnom_v == "") $vide = TRUE;
  if(empty($pw_v = str_replace('"', "", json_encode($req->post('pw')))) || $pw_v == "") $vide = TRUE;
  if(empty($adr_v = str_replace('"', "", json_encode($req->post('adresse')))) || $adr_v == "") $vide = TRUE;
  if(empty($cp_v = str_replace('"', "", json_encode($req->post('cp')))) || $cp_v == "") $vide = TRUE;
  if(empty($vil_v = str_replace('"', "", json_encode($req->post('ville_v')))) || $vil_v == "") $vide = TRUE;
  if(empty($mail_v = str_replace('"', "", json_encode($req->post('mail')))) || $mail_v == "") $vide = TRUE;
  if(empty($tel_v = str_replace('"', "", json_encode($req->post('tel')))) || $tel_v == "") $vide = TRUE;
  if(empty($typ_v = str_replace('"', "", json_encode($req->post('type_v')))) || $typ_v == "") $vide = TRUE;

  if(!$vide)
    {
    $v = Ville::whereNom_vil($vil_a)->first(); // récupération de l'objet Ville
    $q = Quartier::whereNom_qua($qua_a)->first(); // récupération de l'objet Quartier

    // je créé un objet Quartier
    $quartier = new Quartier();
    if(empty($q)) 
      {
      // si le quartier n'existe pas
      // je lui affecte les valeurs
      // je le save = enregistrement bdd
      $quartier->nom_qua = $qua_a;
      $quartier->id_vil = $v->id_vil;
      $quartier->save();
      }
    else
      {
      // sinon je lui transmet les valeurs
      // de l'objet Quartier trouvé
      $quartier->id_qua = $q->id_qua;
      $quartier->nom_qua = $q->nom_qua;
      $quartier->id_vil = $q->id_vil;
      }

    $id_qua = $quartier->id_qua;
    $id_tyb = TypeBien::whereNom_typ($typ_a)->first()->id_typ; // récupération de l'objet Type_Bien

    $mdp_v = md5(trim($pw_v));

    $ven = Vendeur::whereNom_ven($nom_v)->wherePrenom_ven($pnom_v)->first(); // récupération de l'objet Vendeur
    // je créé un objet Vendeur
    $vendeur = new Vendeur();
    if(empty($ven))
      {
      // si le vendeur n'existe pas
      // je lui affecte les valeurs
      // je le save = enregistrement bdd
      $vendeur->pass_ven = $mdp_v;
      $vendeur->numTel_ven = $tel_v;
      $vendeur->adresse_ven = $adr_v.' '.$cp_v.' '.$vil_v;
      $vendeur->mail_ven = $mail_v;
      $vendeur->nom_ven = $nom_v;
      $vendeur->prenom_ven = $pnom_v;
      $vendeur->type_ven = $typ_v;
      $vendeur->save();
      }
    else
      {
      // sinon je lui transmet les valeurs
      // de l'objet Vendeur trouvé
      $vendeur->id_ven = $ven->id_ven;
      $vendeur->pass_ven = $ven->pass_ven;
      $vendeur->numTel_ven = $ven->numTel_ven;
      $vendeur->adresse_ven = $ven->adresse_ven;
      $vendeur->mail_ven = $ven->mail_ven;
      $vendeur->nom_ven = $ven->nom_ven;
      $vendeur->prenom_ven = $ven->prenom_ven;
      $vendeur->type_ven = $ven->type_ven;
      }
    $id_ven = $vendeur->id_ven;

    // récupération de l'objet Annonce
    $ann = Annonce::whereTitre_ann($tit_a)
      ->whereDesc_ann($des_a)
      ->whereId_qua($id_qua)
      ->whereSuperficie_ann($sup_a)
      ->whereNbpiece_ann($nbp_a)
      ->wherePrix_ann($pri_a)
      ->whereId_ven($id_ven)
      ->whereId_typ($id_tyb)
      ->whereLoc_ann($loc_a)
      ->first();

    // je créé un objet Annonce
    $annonce = new Annonce();
    if(empty($ann))
      {
      // si l'annonce n'existe pas
      // je lui affecte les valeurs
      // je le save = enregistrement bdd
      $annonce->titre_ann = $tit_a;
      $annonce->desc_ann = $des_a;
      $annonce->image_ann = 'img/'.strtolower($typ_a).'.png';
      $annonce->id_qua = $quartier->id_qua;
      $annonce->superficie_ann = $sup_a;
      $annonce->nbpiece_ann = $nbp_a;
      $annonce->prix_ann = $pri_a;
      $annonce->id_ven = $id_ven;
      $annonce->id_typ = $id_tyb;
      $annonce->date_ann = date('Y-m-d');
      $annonce->loc_ann = $loc_a;
      $annonce->save();
      }
    else
      {
      // sinon je lui transmet les valeurs
      // de l'objet Annonce trouvé
      $annonce->id_ann = $ann->id_ann;
      $annonce->titre_ann = $ann->titre_ann;
      $annonce->desc_ann = $ann->desc_ann;
      $annonce->image_ann = $ann->image_ann;
      $annonce->id_qua = $ann->id_qua;
      $annonce->superficie_ann = $ann->superficie_ann;
      $annonce->nbpiece_ann = $ann->nbpiece_ann;
      $annonce->prix_ann = $ann->prix_ann;
      $annonce->id_ven = $ann->id_ven;
      $annonce->id_typ = $ann->id_tyb;
      $annonce->date_ann = $ann->date_ann;
      $annonce->loc_ann = $ann->loc_ann;
      }
    $id_ann = $annonce->id_ann;
    header('Location: ../index.php/annonce/'.$id_ann); exit(); // je redirige vers la page qui affichera l'annonce
    }
  else 
    {
    $app->render('dua_create_ven.twig',  array(
      'ville' => Ville::orderBy('nom_vil')->get(),
      'typeBien' => TypeBien::orderBy('nom_typ')->get(),
      'erreur' => 'Un champ est vide.',
      'titre' => $tit_a,
      'desc' => $des_a,
      'qua' => $qua_a,
      'super' => $sup_a,
      'nbp' => $nbp_a,
      'prix' => $pri_a,
      'nom' => $nom_v,
      'pnom' => $pnom_v,
      'pw' => $pw_v,
      'adr' => $adr_v,
      'cp' => $cp_v,
      'mail' => $mail_v,
      'tel' => $tel_v,
    )); // sinon j'affiche le formulaire en affichant l'erreur
    }
  });

$app->post('/ajout_annonce_svendeur', function () use ($app)
  {
  $req = $app->request();

  $vide = FALSE; // boolean TRUE si une valeur est vide ou null.
  /* données annonce */
  if(empty($tit_a = str_replace('"', "", json_encode($req->post('titre')))) || $tit_a == "") $vide = TRUE;
  if(empty($des_a = str_replace('"', "", json_encode($req->post('desc')))) || $des_a == "") $vide = TRUE;
  if(empty($qua_a = str_replace('"', "", json_encode($req->post('quartier')))) || $qua_a == "") $vide = TRUE;
  if(empty($vil_a = str_replace('"', "", json_encode($req->post('ville')))) || $vil_a == "") $vide = TRUE;
  if(empty($sup_a = str_replace('"', "", json_encode($req->post('superficie')))) || $sup_a == "") $vide = TRUE;
  if(empty($nbp_a = str_replace('"', "", json_encode($req->post('nbpiece')))) || $nbp_a == "") $vide = TRUE;
  if(empty($pri_a = str_replace('"', "", json_encode($req->post('prix')))) || $pri_a == "") $vide = TRUE;
  if(empty($typ_a = str_replace('"', "", json_encode($req->post('type')))) || $typ_a == "") $vide = TRUE;
  if(empty($loc_a = str_replace('"', "", json_encode($req->post('loc')))) || $loc_a == "") $vide = TRUE;

  /* données vendeur */
  if(empty($id_ven = str_replace('"', "", json_encode($req->post('vend')))) || $id_ven == "") $vide = TRUE;
  if(empty($pw_v = str_replace('"', "", json_encode($req->post('pwv')))) || $pw_v == "") $vide = TRUE;


  if(!$vide)
    {

    $ven = Vendeur::find($id_ven); // récupération objet Vendeur

    if(md5(trim($pw_v)) == $ven->pass_ven) // test si le password est correcte (encodage md5)
      {
      $v = Ville::whereNom_vil($vil_a)->first(); // récupération objet Ville

      $q = Quartier::whereNom_qua($qua_a)->first(); // récupération objet Quartier

      // je créé un objet Quartier
      $quartier = new Quartier();
      if(empty($q))
        {
        // si le quartier n'existe pas
        // je lui affecte les valeurs
        // je le save = enregistrement bdd
        $quartier->nom_qua = $qua_a;
        $quartier->id_vil = $v->id_vil;
        $quartier->save();
        }
      else
        {
        // sinon je lui transmet les valeurs
        // de l'objet Quartier trouvé
        $quartier->id_qua = $q->id_qua;
        $quartier->nom_qua = $q->nom_qua;
        $quartier->id_vil = $q->id_vil;
        }

      $id_qua = $quartier->id_qua;
      $id_tyb = TypeBien::whereNom_typ($typ_a)->first()->id_typ; // récupération de l'objet Type_Bien

      // récupération de l'objet Annonce
      $ann = Annonce::whereTitre_ann($tit_a)
        ->whereDesc_ann($des_a)
        ->whereId_qua($id_qua)
        ->whereSuperficie_ann($sup_a)
        ->whereNbpiece_ann($nbp_a)
        ->wherePrix_ann($pri_a)
        ->whereId_ven($id_ven)
        ->whereId_typ($id_tyb)
        ->whereLoc_ann($loc_a)
        ->first();

      // je créé un objet Annonce
      $annonce = new Annonce();
      if(empty($ann))
        {
        // si l'annonce n'existe pas
        // je lui affecte les valeurs
        // je le save = enregistrement bdd
        $annonce->titre_ann = $tit_a;
        $annonce->desc_ann = $des_a;
        $annonce->image_ann = 'img/'.strtolower($typ_a).'.png';
        $annonce->id_qua = $quartier->id_qua;
        $annonce->superficie_ann = $sup_a;
        $annonce->nbpiece_ann = $nbp_a;
        $annonce->prix_ann = $pri_a;
        $annonce->id_ven = $id_ven;
        $annonce->id_typ = $id_tyb;
        $annonce->date_ann = date('Y-m-d');
        $annonce->loc_ann = $loc_a;
        $annonce->save();
        }
      else
        {
        // sinon je lui transmet les valeurs
        // de l'objet Annonce trouvé
        $annonce->id_ann = $ann->id_ann;
        $annonce->titre_ann = $ann->titre_ann;
        $annonce->desc_ann = $ann->desc_ann;
        $annonce->image_ann = $ann->image_ann;
        $annonce->id_qua = $ann->id_qua;
        $annonce->superficie_ann = $ann->superficie_ann;
        $annonce->nbpiece_ann = $ann->nbpiece_ann;
        $annonce->prix_ann = $ann->prix_ann;
        $annonce->id_ven = $ann->id_ven;
        $annonce->id_typ = $ann->id_tyb;
        $annonce->date_ann = $ann->date_ann;
        $annonce->loc_ann = $ann->loc_ann;
        }
      $id_ann = $annonce->id_ann;
      header('Location: ../index.php/annonce/'.$id_ann); exit(); // je redirige vers la page qui affichera l'annonce
      } 
    else 
      {
      $app->render('dua_ven.twig',  array(
        'vendeur' => Vendeur::all(), 
        'ville' => Ville::orderBy('nom_vil')->get(),
        'typeBien' => TypeBien::orderBy('nom_typ')->get(),
        'erreur' => 'Mot de passe incorrect.',
        'titre' => $tit_a,
        'desc' => $des_a,
        'qua' => $qua_a,
        'super' => $sup_a,
        'nbp' => $nbp_a,
        'prix' => $pri_a,
        )); // sinon j'affiche le formulaire en affichant l'erreur
      }
    } 
  else 
    {
    $app->render('dua_ven.twig',  array(
        'vendeur' => Vendeur::all(), 
        'ville' => Ville::orderBy('nom_vil')->get(),
        'typeBien' => TypeBien::orderBy('nom_typ')->get(),
        'erreur' => 'Un champ est vide.',
        'titre' => $tit_a,
        'desc' => $des_a,
        'qua' => $qua_a,
        'super' => $sup_a,
        'nbp' => $nbp_a,
        'prix' => $pri_a,
      )); // sinon j'affiche le formulaire en affichant l'erreur
    }
  });


/* TOUTES LES ANNONCES */
$app->get('/toutes_les_annonces', function () use ($app)
  {
  $tab_ann = Annonce::with('quartier', 'typeBien')->orderBy('date_ann', 'desc')->get();
  $app->render('tla.twig', array(
    'annonce' => $tab_ann, 
    'typeBien' => TypeBien::all(),
    'quartier' => Quartier::all(),
    'ville' => Ville::all()
    ));
  });


/* AFFICHER 1 ANNONCE */
$app->get("/annonce/:id", function($id) use ($app)
  {
  $val = Annonce::find($id);
  $app->render('annonce.twig', array(
    'annonce' => $val,
    'quartier' => $val->quartier,
    'typeBien' => $val->typeBien,
    'ville' => $val->quartier->ville,
    'vendeur' => $val->vendeur
    ));
  });


/* FILTRER ANNONCE */
$app->get('/recherche', function () use ($app)
    {
    $app->render('sea.twig', array(
      'quartier' => Quartier::all(),
      'ville' => Ville::all(),
      'typeBien' => TypeBien::all()
      ));
    });

$app->post('/recherche', function() use ($app)
  {
  $req = $app->request();

  /* données filtre */
  $qua = str_replace('"', "", json_encode($req->post('quartier')));
  $typ = str_replace('"', "", json_encode($req->post('typ')));
  $vil = str_replace('"', "", json_encode($req->post('ville')));
  $supinf = str_replace('"', "", json_encode($req->post('supinf')));
  $supsup = str_replace('"', "", json_encode($req->post('supsup')));
  $priinf = str_replace('"', "", json_encode($req->post('prixinf')));
  $prisup = str_replace('"', "", json_encode($req->post('prixsup')));
  $nbpinf = str_replace('"', "", json_encode($req->post('nbpinf')));
  $nbpsup = str_replace('"', "", json_encode($req->post('nbpsup')));
  $loc = str_replace('"', "", json_encode($req->post('loc_vente')));

  $id_qua = Quartier::where('nom_qua', $qua)->first()->id_qua; // je récupère l'id du quartier
  $id_typ = typeBien::where('nom_typ',$typ)->first()->id_typ; // je récupère l'id du type_bien 
  $id_vil = Ville::where('nom_vil',$vil)->first()->id_vil; // je récupère l'id de la ville

  // je récupère un objet annonce
  $ann = Annonce::whereId_qua($id_qua)
    ->whereId_typ($id_typ)
    ->whereBetween('superficie_ann', array($supinf, $supsup))
    ->whereBetween('prix_ann', array($priinf, $prisup))
    ->whereLoc_ann($loc)
    ->whereBetween('nbpiece_ann', array($nbpinf, $nbpsup))
    ->get();

  $app->render('fil.twig', array(
    'quartier' => Quartier::all(),
    'ville' => Ville::all(),
    'typeBien' => TypeBien::all(),
    'filtrequa' => $qua,
    'filtrevil' => $vil,
    'filtretyp' => $typ,
    'filtreloc' => $loc,
    'filtresin' => $supinf,
    'filtressu' => $supsup,
    'filtrepin' => $priinf,
    'filtrepsu' => $prisup,
    'filtrenin' => $nbpinf,
    'filtrensu' => $nbpsup,
    'annonce' => $ann
  ));
});

/* MODIFIER ANNONCE */
$app->post('/annonce/:id/administrer', function ($id) use ($app)
  {
  $req = $app->request();
  $pw = str_replace('"', "", json_encode($req->post('pwann')));

  $ann = Annonce::find($id);
  $pwv = $ann->vendeur->pass_ven;
 
  if(md5($pw) == $pwv)
    $app->render('admin.twig', array(
      'annonce' => $ann,
      'quartier' => $ann->quartier,
      'ville' => $ann->quartier->ville,
      'typeBien' => $ann->typeBien,
      'allville' => Ville::orderBy('nom_vil')->get(),
      'allTyb' => TypeBien::orderBy('nom_typ')->get()
    ));
  else header('Location: ../'.$id); exit();
});

$app->post('/annonce/:id/modifier', function ($id) use ($app)
  {
  $req = $app->request();

  $vide = FALSE; // boolean TRUE si une valeur est vide ou null.
  $ann = Annonce::find($id); // je récupère l'objet annonce

  $val_submit = $req->post('val_env');
  /* données annonce */
  if(empty($tit_a = str_replace('"', "", json_encode($req->post('titre_ann')))) || $tit_a == "") $vide = TRUE;
  if(empty($des_a = str_replace('"', "", json_encode($req->post('desc_ann')))) || $des_a == "") $vide = TRUE;
  if(empty($qua_a = str_replace('"', "", json_encode($req->post('nom_qua')))) || $qua_a == "") $vide = TRUE;
  if(empty($vil_a = str_replace('"', "", json_encode($req->post('ville')))) || $vil_a == "") $vide = TRUE;
  if(empty($sup_a = str_replace('"', "", json_encode($req->post('superficie_ann')))) || $sup_a == "") $vide = TRUE;
  if(empty($nbp_a = str_replace('"', "", json_encode($req->post('nbpiece_ann')))) || $nbp_a == "") $vide = TRUE;
  if(empty($pri_a = str_replace('"', "", json_encode($req->post('prix_ann')))) || $pri_a == "") $vide = TRUE;
  if(empty($typ_a = str_replace('"', "", json_encode($req->post('tyb')))) || $typ_a == "") $vide = TRUE;
  if(empty($loc_a = str_replace('"', "", json_encode($req->post('loc')))) || $loc_a == "") $vide = TRUE;

  if($val_submit == "Supprimer")
    {
    Annonce::destroy($id);
    header('Location: ../../toutes_les_annonces'); exit(); // je redirige vers la page qui affichera l'annonce
    }
  elseif($val_submit == "Modifier")
    {
    if(!$vide)
      {
      $t = TypeBien::whereNom_typ($typ_a)->first(); // je récupére l'objet Type_Bien
      $v = Ville::whereNom_vil($vil_a)->first(); // je récupére l'objet Ville
      $q = Quartier::whereNom_qua($qua_a)->first(); // je récupére l'objet Quartier

      // je créé un objet Quartier
      $quartier = new Quartier();
      if(empty($q))
        {
        // si le quartier n'existe pas
        // je lui affecte les valeurs
        // je le save = enregistrement bdd
        $quartier->nom_qua = $qua_a;
        $quartier->id_vil = $v->id_vil;
        $quartier->save();
        }
      else
        {
        // sinon je lui transmet les valeurs
        // de l'objet Annonce trouvé
        $quartier->id_qua = $q->id_qua;
        $quartier->nom_qua = $q->nom_qua;
        $quartier->id_vil = $q->id_vil;
        }

      // je modifie les valeurs de l'objet annonce avec les données de l'utilisateur
      // j'enregistre les modifications dans la BDD (save)
      $ann->titre_ann = $tit_a;
      $ann->desc_ann = $des_a;
      $ann->id_qua = $quartier->id_qua;
      $ann->image_ann = "img/".$typ_a.".png";
      $ann->superficie_ann = $sup_a;
      $ann->nbpiece_ann = $nbp_a;
      $ann->prix_ann = $pri_a;
      $ann->id_typ = $t->id_typ;
      $ann->loc_ann = $loc_a;
      $ann->save();
      header('Location: ../'.$id); exit(); // je redirige vers la page qui affichera l'annonce
      }
    }
  });
 ?>
