<?php

namespace app\model;

use Illuminate\Database\Eloquent\Model as Eloquent;


/**
 * Classe Annonce
 * extends Illuminate\Database\Eloquent\Model
 *
 * Créé un objet Annonce correspondant à la table Annonce
 */
class Annonce extends Eloquent
  {

  /**
   * ATTRIBUTS
   */

  // Nom de la table
	protected $table = 'Annonce';

  // Clé primaire de la table 
  protected $primaryKey = 'id_ann';

  // Optionnel : timestamps 
  public $timestamps=false;


  /**
   * Construit un objet Annonce
   */
  public function __construct() {}


  /** 
   * Méthode quartier
   * Permet de retouver le quartier en fonction de l'annonce
   *
   * @return Objet Quartier
   */
  public function quartier()
    {
    return $this->belongsTo('app\model\Quartier', 'id_qua');
    }

  /** 
   * Méthode typeBien
   * Permet de retouver le typeBien en fonction de l'annonce
   *
   * @return Objet Type_Bien
   */
  public function typeBien()
    {
    return $this->belongsTo('app\model\Type_Bien', 'id_typ');
    }

  /** 
   * Méthode vendeur
   * Permet de retouver le vendeur en fonction de l'annonce
   *
   * @return Objet Vendeur
   */
  public function vendeur()
    {
    return $this->belongsTo('app\model\Vendeur', 'id_ven');
    }
  }

?>
