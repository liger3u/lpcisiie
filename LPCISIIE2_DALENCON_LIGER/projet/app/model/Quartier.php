<?php

namespace app\model;

use Illuminate\Database\Eloquent\Model as Eloquent;


/**
 * Classe Quartier
 * extends Illuminate\Database\Eloquent\Model
 *
 * Créé un objet Quartier correspondant à la table Quartier
 */
class Quartier extends Eloquent
  {

  /**
   * ATTRIBUTS
   */

  // Nom de la table
	protected $table = 'Quartier';

	// Clé primaire de la table
	protected $primaryKey = 'id_qua';

	// Optionnel : timestamps
	public $timestamps=false;


	/**
   * Construit un objet Quartier
   */
  public function __construct() {}


	/**
	 * Méthode ville
	 * Permet de retouver la ville en fonction du quartier
	 *
	 * @return Objet Ville
	 */
	public function ville()
    {
    return $this->belongsTo('app\model\Ville', 'id_vil');
    }

	/**
   * Méthode annonces
   * Permet de retouver les annonces en fonction du quartier
   *
   * @return Objet Annonce
   */
	public function annonces()
  	{
  	return $this->hasMany('app\model\Annonce', 'id_ann');
  	}
	}

?>
