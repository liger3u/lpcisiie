<?php

namespace app\model;

use Illuminate\Database\Eloquent\Model as Eloquent;


/**
 * Classe Vendeur
 * extends Illuminate\Database\Eloquent\Model
 *
 * Créé un objet Vendeur correspondant à la table Vendeur
 */
class Vendeur extends Eloquent
  {

	/**
	 * ATTRIBUTS
	 */

	// Nom de la table
	protected $table = 'Vendeur';

	// Clé primaire de la table
 	protected $primaryKey = 'id_ven';

 	// Optionnel : timestamps
	public $timestamps=false;


	/**
	 * Construit un objet Vendeur
	 */
	public function __construct() {}

	/**
   * Méthode annonces
   * Permet de retouver les annonces en fonction du vendeur
   *
   * @return Objet Annonce
   */
	public function annonces()
  	{
  	return $this->hasMany('app\model\Annonce', 'id_ann');
  	}
 	}

?>
