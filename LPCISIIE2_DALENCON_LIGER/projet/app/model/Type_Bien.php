<?php

namespace app\model;

use Illuminate\Database\Eloquent\Model as Eloquent;


/**
 * Classe Type_Bien
 * extends Illuminate\Database\Eloquent\Model
 *
 * Créé un objet Type_Bien correspondant à la table Type_Bien
 */
class Type_Bien extends Eloquent
	{

	/**
   * ATTRIBUTS
   */

  // Nom de la table
	protected $table = 'Type_Bien';

	// Clé primaire de la table
 	protected $primaryKey = 'id_typ';

 	// Optionnel : timestamps
  public $timestamps=false;


  /**
	 * Construit un objet Type_Bien
	 */
	public function __construct() {}


	/**
   * Méthode annonces
   * Permet de retouver les annonces en fonction du Type_Bien
   *
   * @return Objet Annonce
   */
	public function annonces()
		{
		return $this->hasMany('app\model\Annonce', 'id_ann');
		}
  }

?>
