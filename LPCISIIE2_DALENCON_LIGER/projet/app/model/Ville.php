<?php

namespace app\model;

use Illuminate\Database\Eloquent\Model as Eloquent;


/**
 * Classe Ville
 * extends Illuminate\Database\Eloquent\Model
 *
 * Créé un objet Ville correspondant à la table Ville
 */
class Ville extends Eloquent
  {

  /**
   * ATTRIBUTS
   */

	// Nom de la table
  protected $table = 'Ville';

  // Clé primaire de la table
	protected $primaryKey = 'id_vil';

	// Optionnel : timestamps
	public $timestamps=false;


	/**
	 * Construit un objet Ville
 	 */
	public function __construct() {}


	/**
   * Méthode quartier
   * Permet de retouver le quartier en fonction de la ville
   *
   * @return Objet Annonce
   */
	public function quartier()
    {
    return $this->hasMany('app\model\Quartier', 'id_qua');
    }
	}

?>
