<?php
use Illuminate\Database\Capsule\Manager as Capsule;

$capsule = new Capsule;
/* Configuration des informations de la BDD */
$capsule->addConnection(array(
    'driver'    => 'mysql',
    'host'      => 'localhost',
    'database'  => 'projet_php',
    'username'  => 'root',
    'password'  => '',
    'charset'   => 'utf8',
    'collation' => 'utf8_general_ci',
    'prefix'    => ''
));
$capsule->setAsGlobal();
$capsule->bootEloquent();

?>
