Prérequis :


extraire le dossier "projet" et le placer sur son hébergement web (wamp, lamp, xamp, ..)

Ligne de commande : se placer au niveau du répertoire projet ( exemple lamp : cd /var/www/html/projet )



### MISE EN PLACE BDD ###

Créer la database (exemple : projet_php)
Insérer le fichier sql : mysql -u NOMUSER -p NOMDATABASE > public/script.sql

exemple de mon cas (connexion en root et database projet_php):
	mysql -u root -p projet_php > public/script.sql

Puis ouvrir le fichier app/config.php
insérer les paramètres de la BDD 
	


### MISE A JOUR COMPOSER ###

Effectuer la commande : composer update
-> Un dossier vendor doit se créer.


####################################################
### Et voilà, la configuration du site est faite ###
####################################################
