<?php
if(isset($_GET['page']) && !empty($_GET['page']))
  {
  $page = $_GET['page'];
  }
else
  {
  $page = "rappel";
  }
?>

<!DOCTYPE html>
<html lanf="fr">
  <head>
    <meta charset="utf-8">
    <title>tuto.css-transform</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/style.css" >
  </head>

  <body>

    <h1 class="header">Tutoriel CSS-TRANSFORM</h1>
    <ul class="nav">
      <li><a href="index.php?page=rappel">Rappel</a></li>
      <li><a href="index.php?page=tuto">Tutoriel</a></li>
      <li><a href="index.php?page=demo">Démo</a></li>
      <li><a href="index.php?page=impnav">Implantation navigateur</a></li>
      <li><a href="index.php?page=ress">Ressources</a></li>
    </ul>

    <div class="container">
      <ul>
        <li><del>1 rappel et un pointeur vers la spécification W3C et une référence de qualité et complète</del></li>
        <li>1 tutoriel expliquant l'intéret et l'utilisation du module css, basé sur une démo</li>
        <li>la démo elle-même, avec les sources</li>
        <li>un point sur l'implantation dans les navigateurs</li>
        <li>une liste de ressources/exemples/tutoriels existant sur le même thème</li>
      </ul>
      <hr>

      <?php
      if(file_exists($page.'.html'))
        include $page.'.html';
      else
        header("Location: index.php");
      ?>
    </div>
